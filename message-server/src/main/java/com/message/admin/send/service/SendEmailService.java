package com.message.admin.send.service;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.message.admin.send.pojo.SendEmail;
import com.system.handle.model.ResponseFrame;

/**
 * send_email的Service
 * @author autoCode
 * @date 2017-12-13 11:15:57
 * @version V1.0.0
 */
@Component
public interface SendEmailService {

	/**
	 * 保存待发送的邮件
	 * @param msgId
	 * @param title
	 * @param content
	 * @param emails	接收的邮箱，多个用;分隔
	 * @param sendTime		发送的时间
	 * @param emailFiles 	邮件附件[多个;分隔]
	 * @return
	 */
	public ResponseFrame saveList(String msgId, String title, String content, String emails, Date sendTime, String emailFiles);
	
	/**
	 * 根据id获取对象
	 * @param id
	 * @return
	 */
	public SendEmail get(String id);

	/**
	 * 分页获取对象
	 * @param sendEmail
	 * @return
	 */
	public ResponseFrame pageQuery(SendEmail sendEmail);
	
	/**
	 * 根据id删除对象
	 * @param id
	 * @return
	 */
	public ResponseFrame delete(String id);

	/**
	 * 根据服务编号获取处理中的列表
	 * @param servNo
	 * @return
	 */
	public List<SendEmail> findIng(String servNo);
	/**
	 * 修改待处理的为进行中
	 * @param servNo
	 * @param num 
	 */
	public void updateWaitToIng(String servNo, Integer num);
	/**
	 * 修改状态
	 * @param id
	 * @param status
	 */
	public void updateStatus(String id, Integer status);
}