package com.message.ui.msg.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.message.admin.send.pojo.SendSms;
import com.message.admin.send.service.SendSmsService;
import com.message.ui.comm.controller.BaseController;
import com.system.handle.model.ResponseFrame;

/**
 * 发送的Sms的Controller
 * @author autoCode
 * @date 2017-12-27 15:24:04
 * @version V1.0.0
 */
@Controller
public class UiSendSmsController extends BaseController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UiSendSmsController.class);

	@Autowired
	private SendSmsService sendSmsService;
	
	/**
	 * 分页获取信息
	 * @return
	 */
	@RequestMapping(value = "/sendSms/f-json/pageQuery")
	@ResponseBody
	public void pageQuery(HttpServletRequest request, HttpServletResponse response,
			SendSms sendSms) {
		ResponseFrame frame = null;
		try {
			frame = sendSmsService.pageQuery(sendSms);
		} catch (Exception e) {
			LOGGER.error("分页获取信息异常: " + e.getMessage(), e);
		}
		writerJson(response, frame);
	}

}