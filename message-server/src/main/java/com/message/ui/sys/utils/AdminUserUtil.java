package com.message.ui.sys.utils;

import com.system.comm.utils.FrameMd5Util;

public class AdminUserUtil {

	public static String encodePassword(String password) {
		return FrameMd5Util.getInstance().encodePassword(password);
	}
	
	public static void main(String[] args) {
		System.out.println(encodePassword("123456"));
	}
}