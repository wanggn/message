package com.message.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.message.MessageConfig;
import com.message.admin.sys.enums.UserGroupRuleStatus;
import com.system.auth.AuthUtil;
import com.system.comm.utils.FrameHttpUtil;

/**
 * 接入用户信息
 * @author yuejing
 * @date 2017年12月5日 上午9:33:24
 */
public class UserGroupRuleTest {

	public static void main(String[] args) throws IOException {
		
		System.out.println("===================== 编辑分组规则 begin =======================");
		String result = saveOrUpdate("test", "sys", "123456",
				UserGroupRuleStatus.OPEN.getCode(), UserGroupRuleStatus.OPEN.getCode(), UserGroupRuleStatus.OPEN.getCode(),
				"18306665600;",
				"yuejing@cjhxfund.com;");
		System.out.println(result);
		System.out.println("===================== 编辑分组规则 end =======================");
		

		
		System.out.println("===================== 获取分组规则 begin =======================");
		result = find("test", "123456", null);
		System.out.println(result);
		System.out.println("===================== 获取分组规则 end =======================");
	}
	
	private static String saveOrUpdate(String sysNo, String groupId, String userId,
			Integer status, Integer emailStatus, Integer smsStatus,
			String recePhone, String receEmail) throws IOException {
		String clientId = MessageConfig.clientId;
		String time = String.valueOf(System.currentTimeMillis());
		String sercret = MessageConfig.sercret;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clientId", clientId);
		params.put("time", time);
		params.put("sign", AuthUtil.auth(clientId, time, sercret));
		
		params.put("sysNo", sysNo);
		params.put("groupId", groupId);
		params.put("userId", userId);
		params.put("status", status);
		params.put("emailStatus", emailStatus);
		params.put("smsStatus", smsStatus);
		params.put("recePhone", recePhone);
		params.put("receEmail", receEmail);
		return FrameHttpUtil.post(MessageConfig.address + "/userGroupRule/saveOrUpdate", params);
	}
	
	private static String find(String sysNo, String userId, String pid) throws IOException {
		String clientId = MessageConfig.clientId;
		String time = String.valueOf(System.currentTimeMillis());
		String sercret = MessageConfig.sercret;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("clientId", clientId);
		params.put("time", time);
		params.put("sign", AuthUtil.auth(clientId, time, sercret));
		
		params.put("sysNo", sysNo);
		params.put("userId", userId);
		params.put("pid", pid);
		return FrameHttpUtil.post(MessageConfig.address + "/userGroupRule/find", params);
	}
}